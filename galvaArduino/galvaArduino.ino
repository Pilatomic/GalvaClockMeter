// Date and time functions using a DS1307 RTC connected via I2C and Wire lib
#include <Wire.h>
#include <RTClib.h>

//This defines the max value used in the software
#define ADC_FULL_SCALE      0x0fff
#define GALVA_VMAX          4500    //Galva full scale voltage (mV)
#define ARDUINO_VCC         5000    //Arduino output voltage (mV)
#define BAUDRATE            9600
#define UART_BEGIN_CHAR     '^'
#define UART_END_CHAR       '\n'
#define UART_ARG_SEPARATOR  ','
#define CMD_STRING_LGTH     2
#define MAX_ARGC            6
#define MAX_PARSER_CPU_TIME 100     //(ms)
#define METER_MODE_TIMEOUT  10000   //(ms)

typedef void (*CommandFunctionPtr)(u8, u16*);

struct CommandFunctionStruct {
    char cmdStr[CMD_STRING_LGTH];
    CommandFunctionPtr function;
};

struct DisplayModeStruct {
    bool isClockMode;
    u32 lastValueTimestamp;
};

DisplayModeStruct displayModeStruct;

RTC_DS1307 rtc;

//##############################################################################
//########                                                              ########
//########                        UART COMMANDS                         ########
//########                                                              ########
//##############################################################################


void printCurrentTime(u8, u16*)
{
    DateTime currTime = rtc.now();
    Serial.print("T=");
    Serial.print(currTime.year(), DEC);
    Serial.print(UART_ARG_SEPARATOR);
    Serial.print(currTime.month(), DEC);
    Serial.print(UART_ARG_SEPARATOR);
    Serial.print(currTime.day(), DEC);
    Serial.print(UART_ARG_SEPARATOR);
    Serial.print(currTime.hour(), DEC);
    Serial.print(UART_ARG_SEPARATOR);
    Serial.print(currTime.minute(), DEC);
    Serial.print(UART_ARG_SEPARATOR);
    Serial.print(currTime.second(), DEC);
    Serial.println();
}


void setCurrentTime(u8, u16* argv)
{
    DateTime currTime =  DateTime(argv[0], argv[1], argv[2], argv[3], argv[4], argv[5]);
    rtc.adjust(currTime);
}

void showValue(u8 argc, u16* argv)
{
    if(argc < 1 || argc > 3)
    {
        Serial.println("ERROR: bad argument count");
    }

    if(argc < 3) argv[2] = 100;
    if(argc < 2) argv[1] = 0;

    writeGalva(argv[0], argv[1], argv[2]);

    displayModeStruct.isClockMode = false;
    displayModeStruct.lastValueTimestamp = millis();
}

CommandFunctionStruct commandFunctionsArray[] =
{
    { "T?", &printCurrentTime   },
    { "T=", &setCurrentTime     },
    { "SV", &showValue          },
};

//##############################################################################
//########                                                              ########
//########                          UART TASK                           ########
//########                                                              ########
//##############################################################################


void intParser(u16* valuePtr, char c)
{
    if(c < '0' || c > '9') return;
    (*valuePtr) *= 10;
    (*valuePtr) += (c & 0x0f);
}

bool testIfCmdStrMatches(char* cmdStr1, char* cmdStr2)
{
    u8 cmdStrIndex;
    for(cmdStrIndex = 0 ; cmdStrIndex < CMD_STRING_LGTH ; cmdStrIndex++)
    {
        if(cmdStr1[cmdStrIndex] != cmdStr2[cmdStrIndex]) break;
    }

    return (cmdStrIndex == CMD_STRING_LGTH);
}

CommandFunctionPtr locateCommandFunction(char* rxCmdStr)
{
    for(u8 i = 0 ; i < (sizeof(commandFunctionsArray) / sizeof(CommandFunctionStruct)) ; i++)
    {
        if(testIfCmdStrMatches(commandFunctionsArray[i].cmdStr, rxCmdStr))
        {
            return commandFunctionsArray[i].function;
        }
    }

    return nullptr;
}

void uartCommandTask() {

    static enum {Parser_Start , Parser_Cmd1, Parser_Cmd2, Parser_Args, Parser_Execute } currParserStage = Parser_Start;
    static struct {
        char cmd[CMD_STRING_LGTH];
        u8 argc;
        u16 argv[MAX_ARGC];
    } cmdStruct;

    //Parser UART buffer
    u32 parserStartTime = millis();
    while(Serial.available()
          && currParserStage != Parser_Execute
          && (millis() - parserStartTime) < MAX_PARSER_CPU_TIME)
    {
        char c = Serial.read();

        if(c == UART_BEGIN_CHAR)
        {
            memset(&cmdStruct, 0x00, sizeof(cmdStruct));
            currParserStage = Parser_Cmd1;
            //Serial.println("BeginChar");
        }
        else
        {

            switch(currParserStage)
            {
            case Parser_Cmd1:
                cmdStruct.cmd[0] = c;
                currParserStage = Parser_Cmd2;
                //Serial.println("Cmd1");
                break;

            case Parser_Cmd2:
                cmdStruct.cmd[1] = c;
                currParserStage = Parser_Args;
                //Serial.println("Cmd2");
                break;

            case Parser_Args:
                //End of command
                if( c == UART_END_CHAR)
                {
                    cmdStruct.argc++; //<- argument count is current argument index + 1
                    currParserStage = Parser_Execute;
                    //Serial.println("EndChar");
                }

                //Argument separator received
                else if( c == UART_ARG_SEPARATOR)
                {
                    if(cmdStruct.argc < MAX_ARGC) cmdStruct.argc++;
                    else currParserStage = Parser_Start;
                }

                //Argument content received
                else
                {
                    intParser(&cmdStruct.argv[cmdStruct.argc] , c);
                }
                break;
            }
        }
    }

    //Execute command
    if(currParserStage == Parser_Execute)
    {
        currParserStage = Parser_Start;
        //Serial.println("Locate func");
        CommandFunctionPtr funcPtrToExecute = locateCommandFunction(cmdStruct.cmd);
        if(funcPtrToExecute)
        {
            //Serial.println("Execute func");
            funcPtrToExecute(cmdStruct.argc, cmdStruct.argv);
        }
        else
        {
            Serial.println("ERROR: unknown command");
        }
    }
}

//##############################################################################
//########                                                              ########
//########                         CLOCK TASK                           ########
//########                                                              ########
//##############################################################################

void clockTask(){
    static DateTime prevTime = DateTime();
    DateTime currTime = rtc.now();

    //Handle clock display
    if(displayModeStruct.isClockMode)
    {
        if(currTime.secondstime() != prevTime.secondstime())
        {
            writeGalva(1, currTime.hour()   , 24);
            writeGalva(2, currTime.minute() , 60);

            prevTime = currTime;
        }
    }

    //Meter mode timeout
    else if(millis() - displayModeStruct.lastValueTimestamp > METER_MODE_TIMEOUT)
    {
        displayModeStruct.isClockMode = true;
    }
}

//##############################################################################
//########                                                              ########
//########                       GALVA CONTROL                          ########
//########                                                              ########
//##############################################################################

void setupPWM16() {
    /* from https://arduino.stackexchange.com/questions/12718/increase-pwm-bit-resolution
    Changing the prescaler will effect the frequency of the PWM signal.
    Frequency[Hz}=CPU/(ICR1+1) where in this case CPU=16 MHz
    16-bit PWM will be>>> 16000000/(65535+1)=244,14Hz
    */

    DDRB |= _BV(PB1) | _BV(PB2);        /* set pins as outputs */
    TCCR1A = _BV(COM1A1) | _BV(COM1B1)  /* non-inverting PWM */
            | _BV(WGM11);                   /* mode 14: fast PWM, TOP=ICR1 */
    TCCR1B = _BV(WGM13) | _BV(WGM12)
            | _BV(CS11);                    /* prescaler 1 */
    ICR1 = (u16) ADC_FULL_SCALE;         /* TOP counter value (freeing OCR1A*/
}

void writeGalva(u8 galva, u16 val, u16 maxVal){
    //Full scale value
    u32 valueToWrite = (((u32)ADC_FULL_SCALE * GALVA_VMAX) / ARDUINO_VCC);

    if(val < maxVal)
    {
        valueToWrite *= val;
        valueToWrite /= maxVal;
    }

    switch(galva)
    {
    case 1:
        OCR1A = valueToWrite;
        break;

    case 2:
        OCR1B = valueToWrite;
        break;

    default:
        break;
    }
}

//##############################################################################
//########                                                              ########
//########                        SETUP & LOOP                          ########
//########                                                              ########
//##############################################################################

void setup() { 
    displayModeStruct.isClockMode = true;

    Serial.begin(BAUDRATE);
    
    if (!rtc.begin())
    {
        Serial.println("Couldn't find RTC");
    }
    
    setupPWM16();
} 


void loop() { 
    uartCommandTask();
    clockTask();
}


